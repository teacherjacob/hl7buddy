#README

HL7Buddy
=========

HL7Buddy is a python application to view, and edit HL7 2.x messages in a user friendly tool. The application is written in Python, and converted to .exe windows application. 
The application feature include: 

  - View window to display the original message
  - Parsed view to display the message's content in a value pair view
  - And Edit mode to allow user to modify the message in real time.
  
Version
----

0.0.1

Package 
-----------
The package used include: 

  - python-hl7
  - PythonQT
    
    
Installation
--------------
The zip contains all the necessary code needed. 
For windows: 
  - Unzip into a folder and run *.exe
For Linux 
  - run *.py file. 

License
----

GNU General Public License


