#!/usr/bin/python
# -*- coding: utf-8 -*-


# Copyright [2014] Jacob Wan.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" This Module parsed take the user input and parsed into user friendly visual tree 

The problem is intended to be executed :   $python main.py  


"""



from PyQt4 import QtCore, QtGui,uic
import webbrowser
import datetime
import socket
import sys
from MainWindow import *
import string 
import hl7

message = 'MSH|^~\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3456|P|2.4\r'
message += 'PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|196203520|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r'
message += 'OBR|1|845439^GHH OE|1045813^GHH LAB|1554-5^GLUCOSE|||200202150730||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^LEVEL SEVEN HEALTHCARE, INC.|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r'
message += 'OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F'

# """  For documentation of the webbrowser module,
# see http://docs.python.org/library/webbrowser.html
# """


# open a public URL, in this case, the webbrowser docs
# url = "http://docs.python.org/library/webbrowser.html"
# webbrowser.open(url)

# open an HTML file on my own (Windows) computer
# url = "file://X:/MiscDev/language_links.html"
# webbrowser.open(url,new=new)

        
class Node(object):
    
    def __init__(self, name, data, parent=None):
        
        self._name = name
        self._data = data
        self._children = []
        self._parent = parent
        
        if parent is not None:
            parent.addChild(self)


    def typeInfo(self):
        return "NODE"

    def addChild(self, child):
        self._children.append(child)

    def insertChild(self, position, child):
        
        if position < 0 or position > len(self._children):
            return False
        
        self._children.insert(position, child)
        child._parent = self
        return True

    def removeChild(self, position):
        
        if position < 0 or position > len(self._children):
            return False
        
        child = self._children.pop(position)
        child._parent = None

        return True
    
    
    def removeAll(self):
        for i in range(len(self._children)):
            self.removeChild(i)
        return True
        

    def name(self):
        return self._name

    def data(self):
        return self._data
    
    def setdata(self, data):
        self._data = data
        
    def setName(self, name):
        self._name = name

    def child(self, row):
        return self._children[row]
    
    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent
    
    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)


    def log(self, tabLevel=-1):

        output     = ""
        tabLevel += 1
        
        for i in range(tabLevel):
            output += "\t"
        
        output += "|------" + self._name + "\n"
        
        for child in self._children:
            output += child.log(tabLevel)
        
        tabLevel -= 1
        output += "\n"
        
        return output

    def __repr__(self):
        return self.log()


"""
    ********Message Model to hold the parsed message model***********
"""
class TreeModel(QtCore.QAbstractItemModel):
    
    """INPUTS: Node, QObject"""
    def __init__(self, root, parent=None):
        super(TreeModel, self).__init__(parent)
        self._rootNode = root

    """INPUTS: QModelIndex"""
    """OUTPUT: int"""
    def rowCount(self, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    """INPUTS: QModelIndex"""
    """OUTPUT: int"""
    def columnCount(self, parent):
        return 2
    
    """INPUTS: QModelIndex, int"""
    """OUTPUT: QVariant, strings are cast to QString which is a QVariant"""
    def data(self, index, role):
        
        if not index.isValid():
            return None

        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if index.column() == 0:
                return node.name()
            if index.column() == 1:
                return node.data()
            
        if role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                return None 
               
         

    """INPUTS: QModelIndex, QVariant, int (flag)"""
    def setData(self, index, value, role=QtCore.Qt.EditRole):

        if index.isValid():
            
            if role == QtCore.Qt.EditRole:
                
                node = index.internalPointer()
                node.setdata(value)
                
                return True
        return False

    
    """INPUTS: int, Qt::Orientation, int"""
    """OUTPUT: QVariant, strings are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if section == 0:
                return "Definition"
            else:
                return "Data"

        
    
    """INPUTS: QModelIndex"""
    """OUTPUT: int (flag)"""
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable

    

    """INPUTS: QModelIndex"""
    """OUTPUT: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        
        node = self.getNode(index)
        parentNode = node.parent()
        
        if parentNode == self._rootNode:
            return QtCore.QModelIndex()
        
        return self.createIndex(parentNode.row(), 0, parentNode)
        
    """INPUTS: int, int, QModelIndex"""
    """OUTPUT: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column and parent node"""
    def index(self, row, column, parent):
        
        parentNode = self.getNode(parent)

        childItem = parentNode.child(row)


        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    
          
    """CUSTOM"""
    """INPUTS: QModelIndex"""
    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node
            
        return self._rootNode

    def addBranch(self, index, childname):
        """Create a new branch under the given parent."""
        
        self.insertRows(0, 1, index)
        child_idx = self.index(0, 0, index)
        self.setData(child_idx, childname)
        return True
    
    """INPUTS: int, int, QModelIndex"""
    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        success = False
        parentNode = self.getNode(parent)
        
        self.beginInsertRows(parent, position, position + rows - 1)
        
        for row in range(rows):
            childCount = parentNode.childCount()
            childNode = Node("untitled" + str(childCount))
            success = parentNode.insertChild(position, childNode)
        
        self.endInsertRows()

        return success
     
    def deleteAll(self):
        self.removeRows(0,self._rootNode.childCount())
       
   
    """INPUTS: int, int, QModelIndex"""
    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        
        parentNode = self.getNode(parent)
        self.beginRemoveRows(parent, position, position + rows - 1)
        
        for row in range(rows):
            success = parentNode.removeChild(position)
            
        self.endRemoveRows()
        
        return success
  
    def nodeFromIndex(self, index):
        """Retrieves the tree node with a given index."""

        if index.isValid():
            return index.internalPointer()
        else:
            return self.root
  
    def deleteNode(self, index):
        """Delete a node from the model."""

        node = self.nodeFromIndex(index)
        # Deletes the node from the tree of databases model/view
        parent = self.parent(index)
        position = node.row()
        self.removeRows(position, 1, parent)
       

   
   
class SendToDiag(QtGui.QDialog):
    
        
    def __init__(self, parent, controller=None):
        super(SendToDiag, self).__init__(parent=parent)
        uic.loadUi('SendToDiag.ui', self)
        self.controller = controller
        self.txtAddress.setText(self.controller.address)
        self.txtPort.setText(self.controller.port)
        self.btnSend.clicked.connect(self.send)
    
    def send(self):
        address = self.txtAddress.text()
        port = self.txtPort.text()
        self.controller.address = address
        self.controller.port = port
        self.txtReply.setPlainText(self.controller.sendTo(address, port))
        
class About(QtGui.QDialog):
    def __init__(self, parent):
        super(About, self).__init__(parent=parent)
        uic.loadUi('About.ui', self)
    
    def mousePressEvent (self, QMouseEvent):
        self.close()
    
    


class HL7Buddy(QtGui.QMainWindow):
    
    
    def __init__(self, controller =None,  parent=None):
    
        super(HL7Buddy, self).__init__()
        uic.loadUi('MainWindow.ui', self)
        self.controller = controller
        self.controller.addView(self)
        self.initUI()


        self.counter = 0
        self.setupMenus()
       
    def setupMenus(self):
        """Set up the main window menus."""
        self.actionImport.triggered.connect(self.selectFile)
        self.actionSave.triggered.connect(self.saveFile)
        self.actionSendTo.triggered.connect(self.openSendToDiag)
        self.actionAbout.triggered.connect(self.openAbout)
        self.actionHelp.triggered.connect(self.openHelp)
        # The tree of databases model/view
    
    def openSendToDiag(self):
        sendto = SendToDiag(self, controller=self.controller)
        sendto.show()
        
    def openAbout(self):
        about = About(self)
        about.show()
        
    def openHelp(self):
#         url = "http://docs.python.org/library/webbrowser.html"
        url = "Help.pdf"
        webbrowser.open(url)
        
    def initUI(self):
        self.txtRawMsg.textChanged.connect(self.txtChanged)
        self.refresh()
        
    def refresh(self):
        self.txtRawMsg.setPlainText(self.controller.message)
        self.txtChanged()
        
    def txtChanged(self):
        self.controller.updateModel(self.txtRawMsg.document())
        model = self.treeParsedMsg.model()
        if model: 
            model.deleteAll()
        self.treeParsedMsg.setModel(self.controller.model)           
    
           
        
    def selectFile(self):
        
        file_path= QtGui.QFileDialog.getOpenFileName()
        self.controller.importFile(file_path)
        print file_path
        
    def saveFile(self):
        file_path= QtGui.QFileDialog.getSaveFileName()
        self.controller.writeFile(file_path)
    
    

  

    def closeEvent(self, event):
        """Handle close events."""
        QtGui.qApp.quit()
    

    def slotRemoveBranch(self):
        """Delete a given branch from the tree model and its views."""

        current = self.ui.treeParsedMsg.currentIndex()
        if current is None:
            return

        # Delete the node
        self.tree_model.deleteNode(current)

  

class Controller(object):
    
    def __init__(self, reference = None, views=[]):
        self.controller_name = ""
        self.address = "127.0.0.1"
        self.port = "9999"
        self.message = message
        self.model = None
        self._reference = reference
        self._views = views
        
    def addView(self,view):
        self._views.append(view)
        
    def refreshViews(self):
        for view in self._views:
            view.refresh()
            
    def updateModel(self, doc):
        self.message  = self.convertToPlainText(doc)
        self.model = self.parseTextToModel(self.message)
        
            
    def convertToPlainText(self, doc):
        toReturn = ""
        for i in range(doc.lineCount()):
            blk = doc.findBlockByNumber(i)         
            toReturn += blk.text()+"\r"       
        return toReturn
    
    def parseTextToModel(self, text):
                           
        h=hl7.parse(text)
        root = Node(name="HL7",data="-")
        for seg in h:
            seg_tag = seg[0][0]
            parent = Node(name=seg_tag,data=(unicode(seg)),parent=root)
            self._reference.segment(seg_tag)
            field_idx = 0 
            for field in seg:
                if field_idx > 0 :
                    field_name = self.getElementName(seg_tag, field_idx, 0)
                    field_data = unicode(field)
                    child = Node(name=field_name, data=unicode(field_data), parent=parent)
                    if self.hasSubField(seg_tag, field_idx):
                        element_idx = 0
                        for element in field:
                            element_name = self.getElementName(seg_tag, field_idx, element_idx+1)
                            element_node = Node(name=element_name, data=unicode(element), parent=child)
                            element_idx+=1
                field_idx+=1 
                
        model = TreeModel(root)
        return model
    
    def hasSubField(self,segment, field_idx):
        if len(self._reference.segment(segment)[field_idx]) > 1:
            return True
        else:
            return False
        
        
    def getElementName(self, segment, field_idx, element_idx):
        if element_idx >= len(self._reference.segment(segment)[field_idx]):
            element_idx = len (self._reference.segment(segment)[field_idx])-1
         
        return self._reference.segment(segment)[field_idx][element_idx]
    
    def importFile(self, file_path):
        message = ""
        fo = open(file_path, "r")
        for line in fo.readlines():
            message+=line.strip()+"\r"
        self.message = message
        self.refreshViews()
#         return self.parseTextToModel(message)
        
    def writeFile(self, file_path):
        fo = open(file_path,"w")
        fo.write(self.message)
    
        
    def sendTo(self, address, port):
        BUFFER_SIZE = 1024
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((address, int(port)))
            s.send(self.message)
            data = s.recv(BUFFER_SIZE)
        except socket.error, (value, message):
            if s:
                s.close()
            i = datetime.datetime.now()
            currentTime = "--> %s--<\n\n" %(i)
            print currentTime+"TCP-IP Connect Error:"+message
            return currentTime+"TCP-IP Connect Error:"+message
        i = datetime.datetime.now()
        currentTime = "--> %s--<\n\n" %(i)
        print "received data:", data
        return currentTime+data

  
def main():
    
    app = QtGui.QApplication(sys.argv)
    with open('HL7_message_reference.dat', 'r') as f:
        reference_text= f.read()
        f.closed       
    
    ref_hl7 = hl7.parse(reference_text)   
    cltr = Controller(reference=ref_hl7)
    main_window = HL7Buddy(controller=cltr)   
    main_window.show()
    
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
